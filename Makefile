FAVICON=favicon
LOGO=logo-thin

all: iris-website/gfx/favicon.svg iris-website/gfx/logo.svg favicon.svg logo.svg
.PHONY: all

iris-logo/%.pdf: iris-logo/%.tex
	cd iris-logo && pdflatex $(notdir $<)

iris-logo/%.svg: iris-logo/%.pdf
	pdf2svg $< $@

iris-website/gfx/favicon.svg: iris-logo/${FAVICON}.svg
	cp $< $@

iris-website/gfx/logo.svg: iris-logo/${LOGO}.svg
	cp $< $@

favicon.svg: iris-logo/${FAVICON}.svg
	cp $< $@

logo.svg: iris-logo/${LOGO}.svg
	cp $< $@

clean:
	rm -f iris-logo/*.aux iris-logo/*.log iris-logo/*.svg iris-logo/*.pdf
	rm -f favicon.svg logo.svg
.PHONY: clean
