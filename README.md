Iris logo and website
=====================

Iris logo
---------

The different versions of the Iris logo can be found in folder `iris-logo`.
The version of the logo / favicon used by the webpage is controlled by the
two variables at the top of the `Makefile`.

To build the logo needed by the weebpage, simply run `make`. Note that the
SVG files used by the Iris website are recorded in the repository.

Iris webpage
------------

To build the Jekyll version of the webpage in folder `iris-webpage` you need
to have a working Ruby installation. Move to the folder, and the first time
you will need to run the `bundle` command. Then, if that succeeds you will be
able to run `bundle exec jekyll serve` in `iris-webpage` folder to be able to
view the website at http://127.0.0.1:4000.

Publication data for the website
--------------------------------

The bibliography data used to generate the webpage are all contained in the
JSON file `iris-website/_data/publications.json`. This is the file you need
to modify if you with to add one of your publications to the webpage.

The file `iris-website/_data/publications.json` contains four lists:
- `publications` containing metadata for all the publications,
- `phd_dissertations` containing metadata for all PhD dissertations,
- `draft_papers` contains metadata for all the draft papers,
- `others` contains metadata for other stuff (MSc theses, talks, ...).

DBLP data (for possible use in the webpage)
-------------------------------------------

Items under `publications` and `phd_dissertations` have, whenever available, a
field `dblp_url` giving the base DBLP url for the entry. It can be used to get
additional metadata from DBLP in several format (by replacing the extension):
- `.html` (as stored in our metadata) gives you the URL to the document's page
  on DBLP, where you can then access all the usual data,
- `.xml` gives you a XML record containing the document's metadata,
- `.bib` gives you a bibtex file for the document. Note that in this case, you
  can use the GET parameter `param` with value `0`, `1`, or `2` to control the
  version of the bibtex you get (condensed, standard, or with crossref).


